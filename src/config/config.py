import os

# Specify the names of the banks
BANKS = [
    'RAIFFEISEN', 
    'ING',
    'REVOLUT'
    ]

# Retrieve environment variables
user     = os.getenv('MYSQL_USER')
password = os.getenv('MYSQL_PASSWORD')
host     = os.getenv('MYSQL_HOST')
database = os.getenv('MYSQL_DATABASE')
# Configure the database connection
DATABASE_URL = f'mysql+mysqlconnector://{user}:{password}@{host}:3306/{database}'

