import pandas as pd
import streamlit as st
import sqlalchemy
from config import *

    
def create_file_uploader(name):
    file = st.file_uploader(f'Upload CSV for {name}', type=['csv'], key=name)
    if file is not None:
        st.success(f'Uploaded {file.name}')
        return file
    

def create_processing_button(engine, files):
    if st.button('Process Files'):
        if all(file is None for file in files):
            st.error('No files uploaded. Please upload at least one file for processing.')
        
        else:
            for file, bank in zip(files, BANKS):
                if file is not None:
                    import_file_to_db(engine, file, bank)
            st.success('Files uploaded to database')
            

def import_file_to_db(engine, file, bank):
    df = pd.read_csv(file, encoding='latin1', sep=';')
    df.to_sql(name=f'{bank}_table', con=engine, if_exists='replace')
    
    
def main():
    # Main page title
    st.title('Expense Tracker')

    # Create each file uploader
    files = [create_file_uploader(bank) for bank in BANKS]

    # Add button to process uploaded files
    engine = sqlalchemy.create_engine(DATABASE_URL)
    create_processing_button(engine, files)
    

if __name__ == "__main__":
    main()