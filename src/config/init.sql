CREATE TABLE IF NOT EXISTS category_mapping (
    category_id INT AUTO_INCREMENT PRIMARY KEY,
    bank_id VARCHAR(255),
    category_name VARCHAR(255),
    keyword VARCHAR(255)
);